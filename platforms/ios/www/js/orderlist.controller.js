angular.module('POS')

.controller("OrderListCtrl", function($scope, $state, $ionicPopover, OrderService, LANG) {
  $scope.orderLists = OrderService.outstandintOrders;
  var orderState = [true, false, false, false];
  var orderStateStr = ["*", "*", "takeout", "internet"];
  var _paidStateStr = {paid: "己付款", unpaid: "未付款"};
  var template = '<ion-popover-view><ion-header-bar> <h1 class="title">My Popover Title</h1> </ion-header-bar> <ion-content> Hello! </ion-content></ion-popover-view>';

   $scope.popover = $ionicPopover.fromTemplate(template, {
     scope: $scope
   });

  $scope.updateOrderStr = "修改訂單";
  $scope.paidStateStr = _paidStateStr.unpaid;
  $scope.match = "*";

  $scope.closePopover = function() {
    $scope.popover.hide();
  };
  //Cleanup the popover when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.popover.remove();
  });

  $scope.changePaidState = function(scope) {
    scope.paidStateStr =
      (scope.paidStateStr === _paidStateStr.unpaid)? _paidStateStr.paid: _paidStateStr.unpaid;
  };

  $scope.updateOrder = function(item, $event) {
    $scope.popover.show($event);
  };

  $scope.selectOrder = function(id) {
    for (var i=0; i<orderState.length; ++i) {orderState[i] = false;};
    $scope.match = orderStateStr[id];
    orderState[id] = true;
  };

  $scope.getTabState = function(id) {
    return orderState[id];
  };
});
