// [
//   {
//      “order_id“:       “20150424-01-000134”,
//      “created_at”:
//      “updated_at”:  “Fri, 24 Apr 2015 03:30:04 UTC +00:00“,
//      “delivery_method”: “local “,
//      "match_method”:    “local“,
//      “aasm_state”:         “unpaid “,
//      “order_items”: [ { “product_name”: ”ppp “ , “quantity”: 8, “ price“: 80 }, { … }, { … }  ]
//    }
// ]

angular.module('POS')

.service("OrderService", function(OrderObj) {
  var mockOrders = [{"delivery_method":"外帶","match_method":"takeout","order_items":{"豬排":{"name":"豬排","img":"img/food/pork_chop.jpg","quantity":2,"category":"rice","price":80},"雞排":{"name":"雞排","img":"img/food/fried_chicken_cutlet.jpg","quantity":2,"category":"rice","price":90}},"subTotal":340},{"delivery_method":"外送","match_method":"internet","order_items":{"蝦捲":{"name":"蝦捲","img":"img/food/shrimp_rolls.jpg","quantity":1,"category":"rice","price":70},"雞腿":{"name":"雞腿","img":"img/food/chicken_leg.jpg","quantity":1,"category":"rice","price":90},"雞排":{"name":"雞排","img":"img/food/fried_chicken_cutlet.jpg","quantity":1,"category":"noodle","price":90}},"subTotal":250},{"delivery_method":"內用","match_method":"local","order_items":{"炒高麗菜":{"name":"炒高麗菜","img":"img/food/stir_fried_cabbage.jpg","quantity":1,"category":"tapas","price":20},"滷豆腐":{"name":"滷豆腐","img":"img/food/marinated_tofu.jpg","quantity":1,"category":"tapas","price":10},"雞排":{"name":"雞排","img":"img/food/fried_chicken_cutlet.jpg","quantity":1,"category":"rice","price":90}},"subTotal":120}, {"delivery_method":"內用","match_method":"local","order_items":{"炒高麗菜":{"name":"炒高麗菜","img":"img/food/stir_fried_cabbage.jpg","quantity":1,"category":"tapas","price":20},"滷豆腐":{"name":"滷豆腐","img":"img/food/marinated_tofu.jpg","quantity":1,"category":"tapas","price":10},"雞排":{"name":"雞排","img":"img/food/fried_chicken_cutlet.jpg","quantity":1,"category":"rice","price":90}},"subTotal":120}, {"delivery_method":"內用","match_method":"local","order_items":{"炒高麗菜":{"name":"炒高麗菜","img":"img/food/stir_fried_cabbage.jpg","quantity":1,"category":"tapas","price":20},"滷豆腐":{"name":"滷豆腐","img":"img/food/marinated_tofu.jpg","quantity":1,"category":"tapas","price":10},"雞排":{"name":"雞排","img":"img/food/fried_chicken_cutlet.jpg","quantity":1,"category":"rice","price":90}},"subTotal":120}];

  this.outstandintOrders = mockOrders;

  this.getOrders = function(idx) {
    ret = [];
    for (var i=0; i<20; ++i) {
      ret.push({
         "order_id":    "20150424-01-000134",
         "created_at":  "",
         "updated_at":  "Fri, 24 Apr 2015 03:30:04 UTC +00:00",
         "delivery_method": "local",
         "match_method":    "local",
         "aasm_state":         "unpaid",
         "order_items": [ { "product_name": "ppp" , "quantity": 8, "price": 80 },
                          { "product_name": "ppp" , "quantity": 8, "price": 80 }]
      });
      return ret;
    }
  };

  this.createOrder = function(obj, cb) {
    // this.outstandintOrders.push(obj);
    cb && cb();
  };
});
