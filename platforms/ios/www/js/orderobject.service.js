angular.module('POS')

.factory("OrderObj", function() {
  function Order(option, match, items, total) {
    this.delivery_method = option;
    this.match_method = match;
    this.order_items = items;
    this.subTotal = total;
  };

  Order.build = function(data) {
    return new Order(data.delivery_method,
                     data.match_method,
                     data.order_items,
                     data.subTotal);
  };

  return Order;
});
