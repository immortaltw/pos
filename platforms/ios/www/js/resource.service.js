angular.module('POS')

.service("ResService", function($rootScope) {
  this.SIDEMENU = {
    en: ["take order", "order list", "KDS"],
    zh: ["點餐", "訂單管理", "KDS"]
  };

  // In take order view
  this.NAV = {
    en: [{ state: "rice", name: "Rice"}, {state: "noodle", name: "Noodle & Soup"}, {state: "tapas", name: "Tapas"}],
    zh: [{ state: "rice", name: "飯"}, {state: "noodle", name: "面 & 湯"}, {state: "tapas", name: "小菜"}]
  };

  this.ORDEROPTION = {
    en: ["To Go", "For Here", "Delivery"],
    zh: ["外帶", "內用", "外送"]
  };

  this.TOTAL = {
    en: "total",
    zh: "總計"
  };

  this.CREATEORDER = {
    en: "Create Order",
    zh: "建立訂單"
  };

  this.MENU = [
    {
      category: "tapas",
      name: {
        en: "Stired Fried Cabbage",
        zh: "炒高麗菜"
      },
      desc: {
        en: "Lorem Ipsum is simply dummy text of the printing.",
        zh: "炒菜"
      },
      price: 20,
      img: "img/food/stir_fried_cabbage.jpg"
    },

    {
      category: "tapas",
      name: {
        en: "Marinated Tofu",
        zh: "滷豆腐"
      },
      desc: {
        en: "Lorem Ipsum is simply dummy text of the printing.",
        zh: "豆腐"
      },
      price: 10,
      img: "img/food/marinated_tofu.jpg"
    },

    {
      category: "rice",
      name: {
        en: "Pork Chop",
        zh: "豬排"
      },
      desc: {
        en: "Lorem Ipsum is simply dummy text of the printing.",
        zh: "大豬排"
      },
      price: 80,
      img: "img/food/pork_chop.jpg"
    },

    {
      category: "rice",
      name: {
        en: "Shrimp Rolls",
        zh: "蝦捲"
      },
      desc: {
        en: "Lorem Ipsum is simply dummy text of the printing.",
        zh: "炸蝦"
      },
      price: 70,
      img: "img/food/shrimp_rolls.jpg"
    },

    {
      category: "tapas",
      name: {
        en: "Braised Egg",
        zh: "滷蛋"
      },
      desc: {
        en: "Lorem Ipsum is simply dummy text of the printing.",
        zh: "香濃滷蛋"
      },
      price: 10,
      img: "img/food/braised_egg.jpg"
    },

    {
      category: "rice",
      name: {
        en: "Chicken Leg",
        zh: "雞腿"
      },
      desc: {
        en: "Lorem Ipsum is simply dummy text of the printing.",
        zh: "大雞腿"
      },
      price: 90,
      img: "img/food/chicken_leg.jpg"
    },

    {
      category: "rice",
      name: {
        en: "Fried Chicken Cutlet",
        zh: "雞排"
      },
      desc: {
        en: "Lorem Ipsum is simply dummy text of the printing.",
        zh: "大雞排"
      },
      price: 90,
      img: "img/food/fried_chicken_cutlet.jpg"
    },

    {
      category: "noodle",
      name: {
        en: "Fried Chicken Cutlet",
        zh: "雞排"
      },
      desc: {
        en: "Lorem Ipsum is simply dummy text of the printing.",
        zh: "大雞排"
      },
      price: 90,
      img: "./img/food/fried_chicken_cutlet.jpg"
    },
  ];
});
