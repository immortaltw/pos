// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('POS', ['ionic'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('pos', {
      url: "/pos",
      abstract: true,
      templateUrl: "partials/menu.html",
      controller: "MainCtrl"
    })

    .state('pos.takeorder', {
      url: "/takeorder/:category",
      views: {
        'menuContent' :{
          templateUrl: "partials/take_order.html",
          controller: "TakeOrderCtrl"
        }
      }
    })

    .state('pos.orderlist', {
      url: "/orderlist",
      views: {
        'menuContent' :{
          templateUrl: "partials/order_list.html",
          controller: "OrderListCtrl"
        }
      }
    })

    .state('pos.kds', {
      url: "/kds",
      views: {
        'menuContent' :{
          templateUrl: "partials/kds.html",
          controller: "KDSCtrl"
        }
      }
    });

  $urlRouterProvider.otherwise("/pos/takeorder/rice");
})

.controller("MainCtrl", function($scope, $rootScope, $state, LANG) {

  $scope.takeOrder = function() {
    $rootScope.$emit("takeOrder", "");
  }
})

.controller("KDSCtrl", function($scope, $state, OrderService, LANG) {
  $scope.displayLists = OrderService.outstandintOrders;
})

.filter("menuFilter", function() {
  return function(items, cat) {
    return items.filter(function(elem, idx, arr) {
      return elem.category === cat;
    });
  };
})

.filter("orderListFilter", function() {
  return function(items, match) {
    return items.filter(function(elem, idx, arr) {
      if (match === "*") return true;
      return elem.match_method === match;
    });
  };
})

.constant('LANG', "zh")
