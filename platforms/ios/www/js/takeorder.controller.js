angular.module('POS')

.controller("TakeOrderCtrl", function($scope, $rootScope, $timeout,
                                      $state, $stateParams, $ionicBackdrop,
                                      $ionicTabsDelegate, $ionicPopup,
                                      CartService, OrderObj, OrderService,
                                      ResService, LANG) {
  var getTabIdByName = function(name) {
    if (name === "rice") return 0;
    else if (name === 'noodle') return 1;
    else return 2;
  };

  var getTabNameById = function(id) {
    if (id === 0) return "rice";
    else if (id === 1) return "noodle";
    else return "tapas";
  };

  var getTitle = function(idx) {
    return $scope.menuTitles[idx].name;
  };

  $timeout(function() {
    $ionicTabsDelegate.select(getTabIdByName($stateParams.category));
  }, 100);

  $rootScope.$on("takeOrder", function(event, data) {
    $timeout(function() {
      $ionicTabsDelegate.select(getTabIdByName($scope.state));
    }, 100);
  });

  $scope.currentItems = CartService.outstandingItemsDict;
  $scope.state = $stateParams.category || "rice";
  $scope.options = ResService.ORDEROPTION[LANG];
  $scope.createOrderStr = ResService.CREATEORDER[LANG];
  $scope.totalStr = ResService.TOTAL[LANG];
  $scope.menuTitles = ResService.NAV[LANG];
  $scope.subTotal = 0;
  $scope.cartNotEmpty = false;
  $scope.selectedOption = {value: 0};

  $scope.rice = getTitle(0);
  $scope.noodle = getTitle(1);
  $scope.tapas = getTitle(2);

  $scope.menu = ResService.MENU.map(function(obj) {
    var newObj = {};
    newObj.name = obj.name[LANG];
    newObj.img = obj.img;
    newObj.quantity = 0;
    newObj.category = obj.category;
    newObj.price = obj.price;
    return newObj;
  });

  $scope.createOrder = function() {
    $ionicBackdrop.retain();
    var alertPopup = $ionicPopup.show({
      title: "Status",
      template: "<h3>Order Created!</h3>"
    });

    $timeout(function() {
       alertPopup.close();
       $ionicBackdrop.release();
    }, 1000);

    OrderService.createOrder(OrderObj.build({
      delivery_method: $scope.options[$scope.selectedOption.value],
      match_method: "",
      order_items: $scope.currentItems,
      subTotal: $scope.subTotal
    }), function() {
      CartService.resetCart();
      $scope.subTotal = CartService.currentSubtotal;
      $scope.currentItems = CartService.outstandingItemsDict;
    });

    $scope.menu.forEach(function(obj) {obj.quantity = 0;});
  };

  $scope.onOptionSelected = function(option, idx) {
    $scope.selectedOption.value = idx;
  };

  $scope.selectTab = function(id) {
    $scope.state = getTabNameById(id);
    $ionicTabsDelegate.select(id);
  };

  $scope.getName = function(item, field) {
    if (item === undefined) return "";
    return item[field][LANG];
  };

  $scope.plus = function(item) {
    item.quantity++;
    CartService.addToCart(item);
    $scope.subTotal = CartService.currentSubtotal;
    $scope.cartNotEmpty = true;
  };

  $scope.minus = function(item) {
    if (item.quantity === 0) return;
    if(item.quantity > 0) item.quantity--;
    CartService.removeFromCart(item);
    console.log(item.quantity);
    $scope.subTotal = CartService.currentSubtotal;
    if (CartService.totalItemCount === 0) $scope.cartNotEmpty = false;
  };
})
