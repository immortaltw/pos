angular.module('POS')

.service("OrderService", function($http) {
  // "/pos/api/get_orders"
  // "/pos/api/cancel_order"
  // "/pos/api/confirm_order"
  // "/pos/api/update_order"
  // "/pos/api/update_orders/"
  // "/pos/api/create_order"
  var _serverAddr = "http://52.68.147.33";

  var _get = function(url, cb) {
    if (!url) return;
    var _url = _serverAddr + url;
    $http({method: 'GET', url: _url, headers: {'Content-Type': '*', 'Access-Control-Allow-Origin': '*'}})
    .success(function(result) {
        cb && cb(result);
    })
    .error(function(result) {
    };
  };

  var _post = function(url, data, cb) {
    $http({method: 'POST', url: url, headers: {'Content-Type': 'application/x-www-form-urlencoded'}, data: $.param(data)})
    .success(function(result) {
        cb && cb(result);
    })
    .error(function(result) {
    }
  };

  
});
