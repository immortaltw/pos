angular.module('POS')

.controller("OrderListCtrl", function($scope, $rootScope, $state, $location,
                                      $ionicTabsDelegate, $ionicHistory,
                                      OrderService, orderList, LANG) {
  var orderState = [true, false, false, false];
  var orderStateStr = [{aasm: true, match: "*"},
                       {aasm: true, match: "order_paid"},
                       {aasm: false, match: "local"},
                       {aasm: false, match: "internet"}];
  var listPageNum = 1;

  $scope.noMoreOrdersAvailable = false;
  $scope.updateOrderStr = "修改訂單";
  $scope.confirmOrderStr = "確認訂單";
  $scope.markAsDeliveringStr = "標記外送中";
  $scope.cancelOrderStr = "取消訂單";

  $scope.match = orderStateStr[0];

  $ionicHistory.nextViewOptions({
    disableBack: true
  });

  $scope.orderLists = OrderService.outstandintOrders || orderList.data.order_collection;
  if(!OrderService.outstandintOrders) {
    OrderService.outstandintOrders = $scope.orderLists;
  }

  $scope.updateOrder = function(item) {
    $state.go("pos.takeorder", {category: "rice"}).then(function() {
      OrderService.prepareToUpdate(item, null);
    });
  };

  $scope.confirmOrder = function(item) {
    console.log(item);
    if (item.aasm_state === "order_paid") return;
    item.aasm_state = "order_paid";
    OrderService.confirmOrder(item);
  };

  $scope.cancelOrder = function(item) {
    if (item.aasm_state === "order_placed") return;
    item.aasm_state = "order_placed";
    OrderService.cancelOrder(item);
  };

  $scope.markAsDelivering = function(item) {
    console.log(item);
  };

  $scope.selectOrder = function(id) {
    for (var i=0; i<orderState.length; ++i) {orderState[i] = false;};
    $scope.match = orderStateStr[id];
    orderState[id] = true;
  };

  $scope.getTabState = function(id) {
    return orderState[id];
  };

  $scope.loadMore = function() {
    OrderService.getOrders(listPageNum+1).then(function(result) {
      if (result.order_collection && result.order_collection.length >0) {
        result.order_collection.forEach(function(obj) {
          $scope.orderLists.push(obj);
        });
      } else {
        $scope.noMoreOrdersAvailable = true;
      }
    });
  };

  $scope.$watch("orderLists", function(newVal, oldVal) {
    $scope.noMoreOrdersAvailable = false;
  });

  $scope.$on("$ionicView.enter", function(event, data) {
    $scope.noMoreOrdersAvailable = false;
  });
});
