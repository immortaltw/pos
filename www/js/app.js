// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('POS', ['ionic'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
    screen.lockOrientation('landscape');
  });
})

.config(function ($httpProvider) {
        delete $httpProvider.defaults.headers.post['Content-type'];
        delete $httpProvider.defaults.headers.common["X-Requested-With"];

        $httpProvider.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
        $httpProvider.defaults.headers.post['Access-Control-Allow-Origin'] = '*';
        // $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
    }
)

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('pos', {
      url: "/pos",
      abstract: true,
      templateUrl: "partials/menu.html",
      controller: "MainCtrl"
    })

    .state('pos.takeorder', {
      url: "/takeorder/:category",
      views: {
        'menuContent' :{
          templateUrl: "partials/take_order.html",
          controller: "TakeOrderCtrl"
        }
      }
    })

    .state('pos.orderlist', {
      url: "/orderlist",
      views: {
        'menuContent' :{
          templateUrl: "partials/order_list.html",
          controller: "OrderListCtrl"
        }
      },
      resolve: {
        orderList: function(OrderService) {
          return OrderService.outstandintOrders || OrderService.getOrders(1);
        }
      }
    })

    .state('pos.kds', {
      url: "/kds/:queueType",
      views: {
        'menuContent' :{
          templateUrl: "partials/kds.html",
          controller: "KDSCtrl"
        }
      },
      resolve: {
        waitingOrders: function(OrderService, $stateParams) {
          var queueType = $stateParams.queueType;
          switch(queueType) {
            case 'local':
              return OrderService.localQueue();
            case 'internet':
              return OrderService.internetQueue();
            case 'all':
            default:
              return OrderService.queueStatus();
          }
        }
      }
    });

  $urlRouterProvider.otherwise("/pos/takeorder/rice");
})

.controller("MainCtrl", function($scope, $rootScope, $state, LANG) {
  $scope.showRightMenu = false;
  $rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams){
    if (fromState.name === "pos.kds") {
      if (toState.name === "pos.kds") $scope.showRightMenu = true;
      else $scope.showRightMenu = false;
    } else if (toState.name === "pos.kds") {
      $scope.showRightMenu = true;
    }
  });

  $scope.takeOrder = function() {
    $rootScope.$emit("takeOrder", "");
  }
})

// .controller("KeypadCtrl", function($scope, $rootScope, $state, LANG) {
//   $scope.keypads = [[1, 2, 3], [4, 5, 6], [7, 8, 9]];
//   console.log("keypad!"); 
// })


.filter("menuFilter", function() {
  return function(items, cat) {
    return items.filter(function(elem, idx, arr) {
      return elem.category === cat;
    });
  };
})

.filter("orderListFilter", function() {
  return function(items, match) {
    return items.filter(function(elem, idx, arr) {
      if (match.match === "*") return true;
      if (match.aasm === true) {
        return elem.aasm_state === match.match;
      } else {
        return elem.match_method === match.match;
      }
    });
  };
})

.constant('LANG', "zh")
