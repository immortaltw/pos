angular.module('POS')

.controller("TakeOrderCtrl", function($scope, $rootScope, $timeout,
                                      $state, $stateParams, $ionicBackdrop,
                                      $ionicTabsDelegate, $ionicPopup, $ionicPopover,
                                      $ionicHistory,
                                      CartService, OrderObj, OrderService,
                                      ResService, LANG) {
  var getTabIdByName = function(name) {
    if (name === "rice") return 0;
    else if (name === 'noodle') return 1;
    else return 2;
  };

  var getTabNameById = function(id) {
    if (id === 0) return "rice";
    else if (id === 1) return "noodle";
    else return "tapas";
  };

  var getTitle = function(idx) {
    return $scope.menuTitles[idx].name;
  };

  $timeout(function() {
    $ionicTabsDelegate.select(getTabIdByName($stateParams.category));
  }, 100);

  $rootScope.$on("takeOrder", function(event, data) {
    $timeout(function() {
      $ionicTabsDelegate.select(getTabIdByName($scope.state));
    }, 100);
  });

  $ionicHistory.nextViewOptions({
    disableBack: true
  });

  $scope.currentItems = CartService.outstandingItemsDict;
  $scope.createOrderStr = ResService.CREATEORDER[LANG];
  $scope.state = $stateParams.category || "rice";
  $scope.options = ResService.ORDEROPTION[LANG];
  $scope.totalStr = ResService.TOTAL[LANG];
  $scope.menuTitles = ResService.NAV[LANG];
  $scope.subTotal = 0;
  $scope.cartNotEmpty = false;
  $scope.selectedOption = {value: 0};
  $scope.diningOption = ["to_go", "stay_in", "delivery"];

  $scope.rice = getTitle(0);
  $scope.noodle = getTitle(1);
  $scope.tapas = getTitle(2);

  $scope.menu = ResService.MENU.map(function(obj) {
    var newObj = {};
    newObj.product_name = obj.product_name[LANG];
    newObj.img = obj.img;
    newObj.quantity = 0;
    newObj.product_id = obj.product_id;
    newObj.category = obj.category;
    newObj.price = obj.price;
    return newObj;
  });

  $scope.plus = function(item) {
    item.quantity++;
    CartService.addToCart(item);
    $scope.subTotal = CartService.currentSubtotal;
    $scope.cartNotEmpty = true;
  };

  $scope.minus = function(item) {
    if (item.quantity === 0) return;
    if(item.quantity > 0) item.quantity--;
    CartService.removeFromCart(item);
    $scope.subTotal = CartService.currentSubtotal;
    if (CartService.totalItemCount === 0) $scope.cartNotEmpty = false;
  };

  // Determine if page is created for updating order
  $scope.isUpdateOrder = (OrderService.orderToBeUpdated !== null);
  var prepareToShowUpdateOrder = function() {
    $scope.cartNotEmpty = true;
    $scope.currentItems = OrderService.itemsToBeUpdated;
    $scope.createOrderStr = ResService.UPDATEORDER[LANG];
    $scope.selectedOption.value = function() {
      if (!$scope.isUpdateOrder) return 0;
      switch(OrderService.orderToBeUpdated.delivery_method) {
        case "to_go": return 0;
        case "stay_in": return 1;
        case "delivery": return 2;
        default: return 0;
      }
    }();
    $scope.menu.forEach(function(obj) {
      if ($scope.currentItems[obj.product_name]) {
        $scope.currentItems[obj.product_name]["product_id"] = obj.product_id;
        for (var i=0; i<$scope.currentItems[obj.product_name].quantity; ++i) {
          $scope.plus(obj);
        }
      }
    });
  };

  $scope.$on("$ionicView.enter", function() {
    $scope.isUpdateOrder = (OrderService.orderToBeUpdated !== null);
    if ($scope.isUpdateOrder) {
      prepareToShowUpdateOrder();
      $ionicTabsDelegate.select(getTabIdByName($stateParams.category));
    }
  });

  var resetView = function() {
    OrderService.resetOrderStatus();
    CartService.resetCart();
    $scope.subTotal = CartService.currentSubtotal;
    $scope.currentItems = CartService.outstandingItemsDict;
    $scope.menu.forEach(function(obj) {obj.quantity = 0;});
    $scope.isUpdateOrder = false;
  };

  $rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams){
    if (fromState.name === "pos.takeorder") {
      resetView();
    }
  });



  $scope.setChecked = function(idx) {
    return idx === $scope.selectedOption.value;
  };

  $scope.createOrder = function() {
    $ionicBackdrop.retain();
    var alertPopup = $ionicPopup.show({
      title: "Status",
      template: "<h3>Order "+($scope.isUpdateOrder)?"Updated!":"Created!"+"</h3>"
    });

    $timeout(function() {
       alertPopup.close();
       $ionicBackdrop.release();
    }, 1000);

    $scope.menu.forEach(function(obj) {
      if ($scope.currentItems[obj.product_name]) {
        $scope.currentItems[obj.product_name].quantity = obj.quantity;
      } else if (obj.quantity>0) {
        $scope.currentItems[obj.product_name] = {
          product_id: obj.product_id,
          product_name: obj.product_name,
          quantity: obj.quantity,
          price: obj.price
        }
      }
    })

    if ($scope.isUpdateOrder) {
      OrderService.updateOrder($scope.currentItems);
      resetView();
      return;
    }

    OrderService.createOrder(OrderObj.build({
      delivery_method: $scope.diningOption[$scope.selectedOption.value],
      match_method: "local",
      order_items: JSON.stringify(Object.keys($scope.currentItems).map(function (obj){var myObj = $scope.currentItems[obj]; return {product_id: myObj["product_id"], quantity: myObj["quantity"]};})),
      subTotal: $scope.subTotal
    }), function() {
      CartService.resetCart();
      $scope.subTotal = CartService.currentSubtotal;
      $scope.currentItems = CartService.outstandingItemsDict;
    });

    $scope.menu.forEach(function(obj) {obj.quantity = 0;});
  };

  $scope.onOptionSelected = function(option, idx) {
    $scope.selectedOption.value = idx;
  };

  $scope.selectTab = function(id) {
    $scope.state = getTabNameById(id);
    $ionicTabsDelegate.select(id);
  };

  $scope.getName = function(item, field) {
    if (item === undefined) return "";
    return item[field][LANG];
  };

  // For popover digit pad
  $ionicPopover.fromTemplateUrl("partials/number_keypad.html", {
    scope: $scope
  }).then(function(popover) {
    $scope.popover = popover;
  });

  $scope.currentItem = null;
  $scope.currentNum = 0;
  $scope.showKeyboard = function(item, $event) {
      $scope.popover.show($event);
      $scope.currentItem = item;
      $scope.currentNum = item.quantity;
  };

  var updateQuantity  = function () {
    var steps = Math.abs($scope.currentNum-$scope.currentItem.quantity);
    var shouldPlus = (($scope.currentNum-$scope.currentItem.quantity) > 0);
    var shouldMinus = (($scope.currentNum-$scope.currentItem.quantity) < 0);
    for (var i=0; i<steps; ++i) {
      if (shouldPlus) {
        $scope.plus($scope.currentItem);
      } 
      if (shouldMinus) {
        $scope.minus($scope.currentItem);
      }
    }   
  };

  $scope.numberPressed = function(num) {
    if (($scope.currentNum*10+num)<100) {
      $scope.currentNum = $scope.currentNum*10+num;
    }
    updateQuantity();
  };

  $scope.deletePressed = function() {
    if (Math.floor($scope.currentNum/10)>=0) {
      $scope.currentNum = Math.floor($scope.currentNum/10);
    }
    updateQuantity();
  };

  $scope.keypads = [[1, 2, 3], [4, 5, 6], [7, 8, 9]];
  $scope.$on('popover.hidden', function() {

  });
})
