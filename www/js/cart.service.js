angular.module('POS')

.service("CartService", function() {
  this.totalItemCount = 0;
  this.currentSubtotal = 0;
  this.outstandingItemsDict = {};

  this.addToCart = function(item) {
    if (!item) return;
    this.totalItemCount += 1;
    this.currentSubtotal += item.price;
    if (this.outstandingItemsDict[item.product_name]) {
      this.outstandingItemsDict[item.product_name].quantity = item.quantity;
  } else this.outstandingItemsDict[item.product_name] = angular.copy(item, this.outstandingItemsDict[item.product_name]);

  };

  this.removeFromCart = function(item) {
    var len = Object.keys(this.outstandingItemsDict).length;
    if (len === 0) return;

    this.totalItemCount -= 1;
    this.currentSubtotal -= item.price;

    if (item.quantity > 0) {
      this.outstandingItemsDict[item.product_name].quantity--;
    } else {
      delete this.outstandingItemsDict[item.product_name];
    }
  };

  this.resetCart = function() {
    this.totalItemCount = 0;
    this.currentSubtotal = 0;
    this.outstandingItemsDict = {};
  };
});
