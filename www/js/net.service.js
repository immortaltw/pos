angular.module('POS')
.service("NetService", function ($http) {
  var createOrderUrl = "pos/api/create_order";
  var getOrderUrl = "pos/api/get_orders";
  var confirmOrderUrl = "pos/api/confirm_order";
  var updateOrderUrl = "pos/api/update_order"; // patch
  var cancelOrderUrl = "pos/api/cancel_order"; // delete

  var queueStatusUrl = "kds/api/queue_status";
  var localQueueUrl = "kds/api/local_queue";
  var internetQueueUrl = "kds/api/internet_queue";

  var _serverAddr = "http://papichang.com.tw/";
  // var _serverAddr = "http://localhost:8100/";

  var _get = function(url, cb) {
    if (!url) return;
    $http({method: 'GET', url: url, headers: {'Content-Type': '*', 'Access-Control-Allow-Origin': '*'}})
    .success(function(result) {
        console.log(result);
        cb && cb(result);
    })
    .error(function(result) {
    });
  };

  var _getPromise = function(url) {
    if (!url) return null;
    var promise = $http({method: 'GET', url: url, headers: {'Content-Type': '*', 'Access-Control-Allow-Origin': '*'}})
    .success(function(result) {
        console.log(result);
    })
    .error(function(result) {
    });
    return promise;
  };

  var _post = function(url, data, cb) {
    if (!url) return;
    $http({
      method: 'POST',
      url: url,
      headers: {'Content-Type': 'application/json'},
      data: data
    })
    .success(function(result) {
        console.log(result);
        cb && cb(result);
    })
    .error(function(result) {
    });
  };

  var _delete = function(url, data, cb) {
    if (!url) return;
    $http({method: 'DELETE', url: url, headers: {'Content-Type': 'application/json'}, data: data})
    .success(function(result) {
        console.log(result);
        cb && cb(result);
    })
    .error(function(result) {
    });
  };

  var _patch = function(url, data, cb) {
    if (!url) return;
    $http({method: 'PATCH', url: url, headers: {'Content-Type': 'application/json'}, data: data})
    .success(function(result) {
      console.log(result);
      cb && cb(result);
    })
    .error(function(result) {
    });
  };

  this.createOrder = function(orderItems, cb) {
    var _url = _serverAddr + createOrderUrl;
    _post(_url, orderItems, cb);
  };

  this.getOrders = function(pageNum) {
    var _url = _serverAddr + getOrderUrl + "?page_number=" + pageNum;
    return _getPromise(_url);
  };

  this.confirmOrder = function(orderId, cb) {
    var _url = _serverAddr + confirmOrderUrl;
    _post(_url, orderId, cb);
  };

  this.cancelOrder = function(orderId, cb) {
    var _url = _serverAddr + cancelOrderUrl;
    _delete(_url, orderId, cb);
  };

  this.updateOrder = function(newOrderItems, cb) {
    var _url = _serverAddr + updateOrderUrl;
    _patch(_url, newOrderItems, cb);
  };

  this.queueStatus = function(cb) {
    var _url = _serverAddr + queueStatusUrl;
    return _getPromise(_url, cb);
  };

  this.localQueue = function(cb) {
    var _url = _serverAddr + localQueueUrl;
    return _getPromise(_url, cb);
  };

  this.internetQueue = function(cb) {
    var _url = _serverAddr + internetQueueUrl;
    return _getPromise(_url, cb);
  };

});
