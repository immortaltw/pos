// [
//   {
//      “order_id“:       “20150424-01-000134”,
//      “created_at”:
//      “updated_at”:  “Fri, 24 Apr 2015 03:30:04 UTC +00:00“,
//      “delivery_method”: “local “,
//      "match_method”:    “local“,
//      “aasm_state”:         “unpaid “,
//      “order_items”: [ { “product_name”: ”ppp “ , “quantity”: 8, “ price“: 80 }, { … }, { … }  ]
//    }
// ]

angular.module('POS')

.service("OrderService", function(OrderObj, NetService) {
  this.outstandintOrders = null;
  this.queueingOrders = null;
  this.orderToBeUpdated = null;
  this.itemsToBeUpdated = {};
  var context = this;

  this.resetOrderStatus = function() {
    this.orderToBeUpdated = null;
    this.itemsToBeUpdated = {};
  };

  this.prepareToUpdate = function(obj, cb) {
    context.orderToBeUpdated = obj;
    obj.order_items.forEach(function(item) {
      context.itemsToBeUpdated[item.product_name] = item;
    });
    console.log("done preparation");
    cb && cb();
  };

  this.getOrders = function(cb, idx) {
    return NetService.getOrders(cb, idx);
  };

  this.createOrder = function(obj, cb) {
    NetService.createOrder(obj, cb);
  };

  this.confirmOrder = function(obj) {
    NetService.confirmOrder({order_id: obj["order_id"]});
  };

  this.cancelOrder = function(obj) {
    NetService.cancelOrder({order_id: obj["order_id"]});
  };

  this.updateOrder = function(obj) {
    context.orderToBeUpdated.order_items = Object.keys(obj).map(function(key) {return obj[key];});
    console.log(context.orderToBeUpdated);
    NetService.updateOrder(context.orderToBeUpdated);
    context.resetOrderStatus();
  };

  this.queueStatus = function(cb) {
    return NetService.queueStatus(function(result) {
      context.queueingOrders = result.waiting_orders;
    });
  };

  this.localQueue = function(cb) {
    return NetService.localQueue(function(result) {
      context.queueingOrders = result.waiting_orders;
    });
  }

  this.internetQueue = function(cb) {
    return NetService.internetQueue(function(result) {
      context.queueingOrders = result.waiting_orders;
    });
  }
});
