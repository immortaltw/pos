angular.module('POS')

.service("ResService", function($rootScope) {
  this.SIDEMENU = {
    en: ["take order", "order list", "KDS"],
    zh: ["點餐", "訂單管理", "KDS"]
  };

  // In take order view
  this.NAV = {
    en: [{ state: "rice", name: "Rice"}, {state: "noodle", name: "Noodle & Soup"}, {state: "tapas", name: "Tapas"}],
    zh: [{ state: "rice", name: "飯"}, {state: "noodle", name: "面 & 湯"}, {state: "tapas", name: "小菜"}]
  };

  this.ORDEROPTION = {
    en: ["To Go", "For Here", "Delivery"],
    zh: ["外帶", "內用", "外送"]
  };

  this.TOTAL = {
    en: "total",
    zh: "總計"
  };

  this.CREATEORDER = {
    en: "Create Order",
    zh: "建立訂單"
  };

  this.UPDATEORDER = {
    en: "Update Order",
    zh: "修改訂單"
  };

  this.MENU = [
    {
      category: "tapas",
      product_name: {
        en: "Stired Fried Cabbage",
        zh: "炒高麗菜"
      },
      desc: {
        en: "Lorem Ipsum is simply dummy text of the printing.",
        zh: "炒菜"
      },
      price: 20,
      product_id: 6,
      img: "img/food/stir_fried_cabbage.jpg"
    },

    {
      category: "tapas",
      product_name: {
        en: "Marinated Tofu",
        zh: "滷豆腐"
      },
      desc: {
        en: "Lorem Ipsum is simply dummy text of the printing.",
        zh: "豆腐"
      },
      price: 10,
      product_id: 7,
      img: "img/food/marinated_tofu.jpg"
    },

    {
      category: "rice",
      product_name: {
        en: "Pork Chop",
        zh: "排骨飯"
      },
      desc: {
        en: "Lorem Ipsum is simply dummy text of the printing.",
        zh: "大豬排"
      },
      price: 49,
      product_id: 1,
      img: "img/food/pork_chop.jpg"
    },
    {
      category: "rice",
      product_name: {
        en: "Sausage",
        zh: "香腸飯"
      },
      desc: {
        en: "Lorem Ipsum is simply dummy text of the printing.",
        zh: "大豬排"
      },
      price: 34,
      product_id: 9,
      img: "img/food/pork_chop.jpg"
    },

    {
      category: "rice",
      product_name: {
        en: "Shrimp Rolls",
        zh: "蝦卷"
      },
      desc: {
        en: "Lorem Ipsum is simply dummy text of the printing.",
        zh: "炸蝦"
      },
      price: 87,
      product_id: 2,
      img: "img/food/shrimp_rolls.jpg"
    },

    {
      category: "tapas",
      product_name: {
        en: "Braised Egg",
        zh: "滷蛋"
      },
      desc: {
        en: "Lorem Ipsum is simply dummy text of the printing.",
        zh: "香濃滷蛋"
      },
      price: 10,
      product_id: 8,
      img: "img/food/braised_egg.jpg"
    },

    {
      category: "rice",
      product_name: {
        en: "Chicken Leg",
        zh: "雞腿"
      },
      desc: {
        en: "Lorem Ipsum is simply dummy text of the printing.",
        zh: "大雞腿"
      },
      price: 90,
      product_id: 3,
      img: "img/food/chicken_leg.jpg"
    },

    {
      category: "rice",
      product_name: {
        en: "Fried Chicken Cutlet",
        zh: "雞排"
      },
      desc: {
        en: "Lorem Ipsum is simply dummy text of the printing.",
        zh: "大雞排"
      },
      price: 90,
      product_id: 4,
      img: "img/food/fried_chicken_cutlet.jpg"
    },

    {
      category: "noodle",
      product_name: {
        en: "Fried Chicken Cutlet",
        zh: "雞排"
      },
      desc: {
        en: "Lorem Ipsum is simply dummy text of the printing.",
        zh: "大雞排"
      },
      price: 90,
      product_id: 5,
      img: "./img/food/fried_chicken_cutlet.jpg"
    },
  ];
});
