angular.module('POS')

.controller("KDSCtrl", function ($scope, $ionicHistory, OrderService, waitingOrders) {
  	$scope.displayLists = waitingOrders.data.waiting_orders;
	$ionicHistory.nextViewOptions({
    	disableBack: true
  	});
  });
